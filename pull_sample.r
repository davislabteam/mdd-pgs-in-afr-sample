## pull african depression case/control numbers for PGS project with Karmel
setwd("/data/davis_lab/sealockj/projects/afr_mdd_pgs/")

library(data.table)
afr_geno <- read.table("/data/davis_lab/shared/genotype_data/biovu/processed/imputed/best_guess/MEGA/MEGA_recalled/20200515_biallelic_mega_recalled.chr1-22.grid.AA.filt1.r2corrected.fam", header=F, sep="")
icd <- fread("/data/davis_lab/shared/phenotype_data/biovu/delivered_data/sd_wide_pull/20200601_pull/icd_202006_repull_all_data.csv.gz", header=T)
icd.afr <- icd[(icd$GRID %in% afr_geno$V2),]
icd.afr2 <- icd.afr[, c("GRID","ICD_DATE","ICD_CODE")]

mdd.icd9 <- c("296.2", "296.20", "296.21", "296.22", "296.23", "296.24", "296.25", "296.26", "296.3", "296.30", "296.31", "296.32", "296.33", "296.34", "296.35", "296.36", "311")
mdd.icd10 <- c("F32", "F32.0", "F32.1", "F32.2", "F32.3", "F32.4", "F32.5", "F32.9", "F33", "F33.0", "F33.1", "F33.2", "F33.3", "F33.40", "F33.41", "F33.42", "F33.9")
mdd.icd <- c(mdd.icd9, mdd.icd10)

### controls
## controls
any_mdd_code <- icd.afr2[(icd.afr2$ICD_CODE %in% mdd.icd),]
mdd_controls <- icd.afr2[!(icd.afr2$GRID %in% any_mdd_code$GRID),]
nrow(mdd_controls[!duplicated(mdd_controls$GRID),])
#[1] 12670

## subset to at least 40 at last code
mdd_controls_last_code <- setDT(mdd_controls)[order(-ICD_DATE), head(.SD,1L),by="GRID"]
static <- fread("/data/davis_lab/shared/phenotype_data/biovu/delivered_data/sd_wide_pull/20200601_pull/static/2020-06.csv.gz", header=T)
dob <- static[, c("GRID","DOB")]
mdd_controls_last_code_dob <- merge(mdd_controls_last_code, dob, by="GRID")
mdd_controls_last_code_dob$age_at_last_code <- as.numeric(as.Date(mdd_controls_last_code_dob$ICD_DATE, "%Y-%m-%d") - as.Date(mdd_controls_last_code_dob$DOB, "%Y-%m-%d"))/365.25
mdd_controls_last_code_dob_as_least_40 <- subset(mdd_controls_last_code_dob, age_at_last_code>=40)
nrow(mdd_controls_last_code_dob_as_least_40)
#[1] 6424

###### Case Definition 1
### depression cases = 2+ depression codes in any setting
any_mdd_code <- icd.afr2[(icd.afr2$ICD_CODE %in% mdd.icd),]
any_mdd_code_count <- any_mdd_code %>% count(GRID)
mdd_cases <- subset(any_mdd_code_count, n>=2)
mdd_cases$n <- NULL
mdd_cases$MDD <- "TRUE"

mdd_controls <- mdd_controls_last_code_dob_as_least_40[, c("GRID","MDD")]

all_codes_mdd_cases_controls <- rbind(mdd_cases, mdd_controls)

summary(as.factor(all_codes_mdd_cases_controls$MDD))
#FALSE  TRUE
# 6424  1822

write.table(all_codes_mdd_cases_controls, "afr_sample_mdd_cases_controls_using_all_codes_05-03-2021.txt", col.names=T, row.names=F, quote=F, sep="\t")

###### Case Definition 2
### depression cases = 2+ depression outpt ICD codes or 1+ inpt depression codes
### depression controls = no depression codes and 40+ age

## subset to depression icd codes
visit <- fread("/data/davis_lab/shared/phenotype_data/biovu/delivered_data/sd_wide_pull/202012_pull_visit_data/visit_details.csv.gz", header=T)
visit.afr <- visit[(visit$grid %in% afr_geno$V2),]
visit.afr2 <- visit.afr[, c("grid","visit_start_date","visit_end_date","visit_type")]

## subset to inpt dates
inpt <- subset(visit.afr2, visit_type=="Inpatient Visit")
nrow(subset(inpt, visit_end_date==""))
[1] 16354 ## no end date for inpt 

icd_inpt <- merge(inpt, icd.afr2, by.x="grid", by.y="GRID", allow.cartesian=TRUE)
icd_inpt$ICD_DATE <- as.Date(icd_inpt$ICD_DATE, "%Y-%m-%d")
icd_inpt$visit_start_date <- as.Date(icd_inpt$visit_start_date, "%Y-%m-%d")
icd_inpt$visit_end_date <- as.Date(icd_inpt$visit_end_date, "%Y-%m-%d")

icd_during_inpt <- subset(icd_inpt, ICD_DATE>=visit_start_date & ICD_DATE<=visit_end_date)

## subset to outpt, er, and other 
outpt <- subset(visit.afr2, visit_type=="Outpatient Visit" | visit_type=="Emergency Room Visit" | visit_type=="No matching concept")

icd_outpt <- merge(outpt, icd.afr2, by.x="grid", by.y="GRID", allow.cartesian=TRUE)
icd_outpt$ICD_DATE <- as.Date(icd_outpt$ICD_DATE, "%Y-%m-%d")
icd_outpt$visit_start_date <- as.Date(icd_outpt$visit_start_date, "%Y-%m-%d")
icd_outpt$visit_end_date <- as.Date(icd_outpt$visit_end_date, "%Y-%m-%d")

icd_during_outpt <- subset(icd_outpt, ICD_DATE>=visit_start_date & ICD_DATE<=visit_end_date)


## inpt
icd_during_inpt_mdd_cases <- icd_during_inpt[(icd_during_inpt$ICD_CODE %in% mdd.icd),]
icd_during_inpt_mdd_cases$MDD <- "TRUE"
icd_during_inpt_mdd_cases$setting <- "in-pt"
nrow(icd_during_inpt_mdd_cases[!duplicated(icd_during_inpt_mdd_cases$grid),])
#[1] 1299

## outpt 
icd_during_outpt_mdd_cases <- icd_during_outpt[(icd_during_outpt$ICD_CODE %in% mdd.icd),]
nrow(icd_during_outpt_mdd_cases[!duplicated(icd_during_outpt_mdd_cases$grid),])
#[1] 2189

library(dplyr)
icd_during_outpt_mdd_cases_count <- icd_during_outpt_mdd_cases %>% count(grid)
icd_during_outpt_mdd_cases_count_at_least2 <- subset(icd_during_outpt_mdd_cases_count, n>=2)
icd_during_outpt_mdd_cases2 <- icd_during_outpt_mdd_cases[(icd_during_outpt_mdd_cases$grid %in% icd_during_outpt_mdd_cases_count_at_least2$grid),]
nrow(icd_during_outpt_mdd_cases2[!duplicated(icd_during_outpt_mdd_cases2$grid),])
#[1] 1713

icd_during_outpt_mdd_cases2$MDD <- "TRUE"
icd_during_outpt_mdd_cases2$setting <- "out-pt"

## combine cases
icd_during_inpt_mdd_cases2 <- icd_during_inpt_mdd_cases[, c("grid","MDD","setting")]
icd_during_outpt_mdd_cases3 <- icd_during_outpt_mdd_cases2[, c("grid","MDD","setting")]
icd_during_inpt_mdd_cases2_dedup <- icd_during_inpt_mdd_cases2[!duplicated(icd_during_inpt_mdd_cases2),]
icd_during_outpt_mdd_cases3_dedup <- icd_during_outpt_mdd_cases3[!duplicated(icd_during_outpt_mdd_cases3),]

nrow(icd_during_inpt_mdd_cases2_dedup)
#[1] 1299
nrow(icd_during_outpt_mdd_cases3_dedup)
#[1] 1713

cases <- rbind(icd_during_outpt_mdd_cases3_dedup, icd_during_inpt_mdd_cases2_dedup)
cases_dedup <- cases[!duplicated(cases$grid),]
nrow(cases_dedup)
#[1] 2204

## add controls
mdd_controls_last_code_dob_as_least_40$MDD <- "FALSE"
mdd_controls_last_code_dob_as_least_40$setting <- "NA"
mdd_controls_last_code_dob_as_least_40.2 <- mdd_controls_last_code_dob_as_least_40[, c("GRID","MDD","setting")]
colnames(mdd_controls_last_code_dob_as_least_40.2)[1] <- "grid"
mdd_controls_last_code_dob_as_least_40_dedup <- mdd_controls_last_code_dob_as_least_40.2[!duplicated(mdd_controls_last_code_dob_as_least_40.2$grid),]

mdd_case_control_with_inpt_desig <- rbind(cases_dedup, mdd_controls_last_code_dob_as_least_40_dedup)
summary(as.factor(mdd_case_control_with_inpt_desig$MDD))
#FALSE  TRUE
# 6424  2204
write.table(mdd_case_control_with_inpt_desig, "afr_sample_mdd_cases_controls_with_inpt_designation_05-03-2021.txt", col.names=T, row.names=F, quote=F, sep="\t")



## get demographics
demo <- static[, c("GRID","DOB","GENDER")]

get_demo <- function(dat=dat){
    today <- Sys.Date()
    dat_demo <- merge(dat, demo, by="GRID")
    dat_demo$age_today <- as.numeric(today - as.Date(dat_demo$DOB, "%Y-%m-%d"))/365.25
    cases <- subset(dat_demo, MDD=="TRUE")
    controls <- subset(dat_demo, MDD=="FALSE")

    demo <- function(dat=dat, mdd_status=mdd_status){
        percent_female <- (nrow(subset(dat, GENDER=="F"))/nrow(dat))*100
        mean_age <- mean(dat$age_today)
        sd_age <- sd(dat$age_today)
        out <- data.frame(mdd_status, percent_female, mean_age, sd_age)
        return(out)
    }

    cases_demo <- demo(dat=cases, mdd_status="case")
    controls_demo <- demo(dat=controls, mdd_status="controls")
    out <- rbind(cases_demo, controls_demo)
    return(out)
}

all_codes_demo <- get_demo(dat=all_codes_mdd_cases_controls)
inpt_outpt_desig_demo <- get_demo(dat=mdd_case_control_with_inpt_desig)

all_codes_demo$version <- "all codes"
inpt_outpt_desig_demo$version <- "inpt/outpt designation"

mdd_demo <- rbind(all_codes_demo, inpt_outpt_desig_demo)
write.table(mdd_demo, "demographics_for_afr_mdd_case_controls_05-03-2021.txt", col.names=T, row.names=F, quote=F, sep="\t")
